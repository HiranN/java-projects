import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.*;



public class ArrowKeys {
	
	public ArrowKeys() {
		JFrame frame = new JFrame();
		frame.setVisible(true); //This first property just set the GUI to be visible
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //It says what happens when you click the close button "Says the swing to close the window"
		frame.setSize(400, 400);
		frame.setFocusable(true); //gain focus once the Frame is displayed.
		
		JPanel panel = new JPanel();
		JLabel up = new JLabel();
		JLabel down = new JLabel();
		JLabel left = new JLabel();
		JLabel right = new JLabel();
		
		panel.add(up);
		panel.add(down);
		panel.add(right);
		panel.add(left);
		
		up.setText("Up: 0");
		down.setText("Down: 0");
		right.setText("Right: 0");
		left.setText("Left: 0");
		
		frame.addKeyListener(new KeyListener() {
			
			int upCount = 0;
			int downCount = 0;
			int rightCount = 0;
			int leftCount = 0;

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void keyPressed(KeyEvent e) {
				int KeyCode = e.getKeyCode();
				switch(KeyCode) {
					case KeyEvent.VK_UP: //If its the up key then we should do this stuff
						up.setText("Up: " + Integer.toString(upCount++)); //Increment the counter for up ++ //Convert integer in to a String
						break;
					case KeyEvent.VK_DOWN: //If its the down key then we should do this stuff
						down.setText("Down: " + Integer.toString(downCount++)); //Increment the counter for down ++ //Convert integer in to a String
						break;
					case KeyEvent.VK_RIGHT: //If its the right key then we should do this stuff
						right.setText("Right: " + Integer.toString(rightCount++)); //Increment the counter for right ++ //Convert integer in to a String
						break;
					case KeyEvent.VK_LEFT: //If its the left key then we should do this stuff
						left.setText("Left: " + Integer.toString(leftCount++)); //Increment the counter for left ++ //Convert integer in to a String
						break;
					
				}
				
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
		frame.add(panel);
	}

	public static void main(String[] args) {
		new ArrowKeys();
		
		
		

	}

}
