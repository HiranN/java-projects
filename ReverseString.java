
public class ReverseString {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String r = reverse("Pomegranites are amazing!!!!");  //when executing we run the 
		System.out.println(r);

	}
	
	public static String reverse (String s) {  //the String s is "Pomegranites are amazing!!!!"
		char[] letters = new char[s.length()]; //declared an array called letters with the size of the string s
		
		int letterIndex = 0; //letterindex which start with 0
		
		for(int i = s.length() - 1; i >= 0; i--) { // i is decrementing 
			letters[letterIndex] = (s.charAt(i)); //letters of 0 is the character is s.char(i) the character at letter 27
			letterIndex++;   //So we take the ! mark and put it to the first index of the letters character Array
		}
		
		String reverse = "";
		for(int i = 0; i < s.length(); i++) {
			reverse = reverse + letters[i];
		}
		
		
		return reverse;
	}

}
