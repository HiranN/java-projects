
import java.util.Scanner;

public class Pattern {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println("Hello! how many stars would you like ?"); //Print message
		Scanner scan = new Scanner(System.in); //Get input from the User
		int numOfStars = scan.nextInt(); //Created an integer "numOfStars" and it takes the integer that the user enter
		
		for(int i = 1; i <= numOfStars; i++) { //Decides how many rows
			for(int j = 0; j < i; j++) {
				System.out.print("*"); //Each time it prints star it also print a new line //Decides how many columns
			}
			System.out.println();
			
			
		}
		
		for(int i = numOfStars - 1; i > 0; i--) {
			for(int j = 0; j < i; j++) {
				System.out.print("*"); //Each time it prints star it also print a new line //Decides how many columns
			}
			System.out.println();
		}

	}

}
