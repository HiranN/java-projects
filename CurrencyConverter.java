import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Color;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CurrencyConverter {

	private JFrame frame;
	private JTextField txtAmount;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CurrencyConverter window = new CurrencyConverter();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public CurrencyConverter() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 582, 480);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Currency converter");
		lblNewLabel.setBackground(new Color(255, 30, 20));
		lblNewLabel.setForeground(new Color(23, 33, 255));
		lblNewLabel.setFont(new Font("Lucida Grande", Font.PLAIN, 29));
		lblNewLabel.setBounds(137, 21, 364, 44);
		frame.getContentPane().add(lblNewLabel);
		
		JPanel panel = new JPanel();
		panel.setBounds(44, 92, 488, 234);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel_1 = new JLabel("Enter the Amount");
		lblNewLabel_1.setFont(new Font("Lucida Grande", Font.PLAIN, 16));
		lblNewLabel_1.setBounds(36, 53, 172, 16);
		panel.add(lblNewLabel_1);
		
		JLabel lblNewLabel_1_1 = new JLabel("From");
		lblNewLabel_1_1.setFont(new Font("Lucida Grande", Font.PLAIN, 16));
		lblNewLabel_1_1.setBounds(36, 105, 172, 16);
		panel.add(lblNewLabel_1_1);
		
		JLabel lblNewLabel_1_2 = new JLabel("To");
		lblNewLabel_1_2.setFont(new Font("Lucida Grande", Font.PLAIN, 16));
		lblNewLabel_1_2.setBounds(36, 160, 172, 16);
		panel.add(lblNewLabel_1_2);
		
		txtAmount = new JTextField();
		txtAmount.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtAmount.setBounds(211, 42, 163, 36);
		panel.add(txtAmount);
		txtAmount.setColumns(10);
		
		JComboBox txtFrom = new JComboBox();
		txtFrom.setModel(new DefaultComboBoxModel(new String[] {"USD"}));
		txtFrom.setBounds(211, 102, 163, 27);
		panel.add(txtFrom);
		
		JComboBox txtTo = new JComboBox();
		txtTo.setModel(new DefaultComboBoxModel(new String[] {"Indian Rupees", "Srilankan Rupees"}));
		txtTo.setBounds(211, 157, 163, 27);
		panel.add(txtTo);
		
		JButton btnNewButton = new JButton("Convert");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				double tot;
				
				double amount = Double.parseDouble(txtAmount.getText()); //The amount entered to the textField its assigned to the variable "amount"
				
			 if(txtFrom.getSelectedItem().toString() == "USD" && txtTo.getSelectedItem().toString() == "Indian Rupees")
			 {
				 tot = amount * 71.00;
				 
				 JOptionPane.showMessageDialog(null,"Your Amount will be " + String.valueOf(tot)); //Concatenate Operator
			 }
			 else if(txtFrom.getSelectedItem().toString() == "USD" && txtTo.getSelectedItem().toString() == "Srilankan Rupees") 
			 {
				 tot = amount * 200.00;
				 
				 JOptionPane.showMessageDialog(null,"Your Amount will be " + String.valueOf(tot));
			 }
				
				
			}
		});
		btnNewButton.setBackground(new Color(255, 253, 36));
		btnNewButton.setBounds(229, 355, 117, 52);
		frame.getContentPane().add(btnNewButton);
	}
}
