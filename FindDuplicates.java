
public class FindDuplicates {

	public static void main(String[] args) {
		
		
		String sentence = "How many duplicates are there ?";
		System.out.println(sentence);
		
		String characters = ""; // Strings to keep track of duplicate characters
		String duplicates = ""; //Each time instead of printing the character out will just add it to the character's string
		
		for(int i = 0; i < sentence.length(); i++) {
			
			String current = Character.toString(sentence.charAt(i)); //Converted that character in to a String so we can see if it contains it
			
			//We need to see if a character is in there more than once
			
			if (characters.contains(current)) {
				if(!duplicates.contains(current)) //If the character hasn't seen before then it adds to the duplicates string
				duplicates += current + ",";
				
			}
			characters += current; //add each character to the character's string
			
		}
		
		System.out.println(duplicates);

	}

}

