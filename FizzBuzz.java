
public class FizzBuzz {

	public static void main(String[] args) {
		

		for(int number = 1; number < 20 + 1; number++) {
			if(number % 15 == 0) {               //If number is divisible with 15 it outs "FizzBuzz"
				System.out.println("FizzBuzz");
			}else if (number % 3 == 0) {		//If number is divisible with 3 it outs "Fizz"
				System.out.println("Fizz");
			}else if (number % 5 == 0) {		//If number is divisible with 15 it outs "Buzz"
				System.out.println("Buzz");
			}else {
				System.out.println(number);		//Else it continues to print the numbers
			}
		}

	}

}
