import java.awt.EventQueue;
import java.sql.*;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import net.proteanit.sql.DbUtils;

import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class JavaCrud {

	private JFrame frame;
	private JTextField txtprice;
	private JTextField txtbname;
	private JTextField txtedition;
	private JTable table;
	private JTextField txtbid;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JavaCrud window = new JavaCrud();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public JavaCrud() {
		initialize();
		Connect(); //When the form is Initialized along with the connection will be initialized 
		table_load();
	}
	
	Connection con; //All the things in the database are reside on this object "con"
	PreparedStatement pst;
	ResultSet rs; //This class also reside on the sql
	
	
	public void Connect()
		
	{
		try {
			Class.forName("com.mysql.cj.jdbc.Driver"); //Register the Mysql jdbc driver
			con = DriverManager.getConnection("jdbc:mysql://localhost/JavaCrud", "root",""); //Path where the database is reside on, in this case on "localhost"
		}
		catch (ClassNotFoundException ex)
	    {
			
	    }
		catch (SQLException ex)	
		{
	
		}
	}
	
	public void table_load() //When the form is loaded we have to call this method
	{
		try
		{
			pst = con.prepareStatement("select * from book"); //Load all the data to the JTable
			rs = pst.executeQuery(); //All the query will be loaded and assign to ResultSet object
			table.setModel(DbUtils.resultSetToTableModel(rs));
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	
	

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 934, 586);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Book Shop");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblNewLabel.setBounds(394, 34, 245, 76);
		frame.getContentPane().add(lblNewLabel);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Registration", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(25, 122, 401, 203);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel_1 = new JLabel("Book Name");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel_1.setBounds(42, 44, 81, 24);
		panel.add(lblNewLabel_1);
		
		JLabel lblNewLabel_1_1 = new JLabel("Edition");
		lblNewLabel_1_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel_1_1.setBounds(42, 80, 81, 24);
		panel.add(lblNewLabel_1_1);
		
		JLabel lblNewLabel_1_1_1 = new JLabel("Price");
		lblNewLabel_1_1_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel_1_1_1.setBounds(42, 116, 81, 24);
		panel.add(lblNewLabel_1_1_1);
		
		txtprice = new JTextField();
		txtprice.setBounds(135, 113, 207, 32);
		panel.add(txtprice);
		txtprice.setColumns(10);
		
		txtbname = new JTextField();
		txtbname.setColumns(10);
		txtbname.setBounds(135, 41, 207, 32);
		panel.add(txtbname);
		
		txtedition = new JTextField();
		txtedition.setColumns(10);
		txtedition.setBounds(135, 77, 207, 32);
		panel.add(txtedition);
		
		JButton btnNewButton = new JButton("Save");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String bname,edition,price; //Every text added to the text boxes are inserted to this variables
				
				bname = txtbname.getText(); 
				edition = txtedition.getText();
				price = txtprice.getText();
				
				try {
						pst = con.prepareStatement("insert into book(name,edition,price)values(?,?,?)"); //Code for insert the records in to the database
						pst.setString(1, bname);
						pst.setString(2, edition);
						pst.setString(3, price);
						pst.executeUpdate();
						JOptionPane.showMessageDialog(null, "Record Addeddd!!!!!");
						table_load(); //After is the successful message the tabl_load() method will be loaded, means the table should be refreshed
						
						txtbname.setText("");
						txtedition.setText("");
						txtprice.setText("");
						txtbname.requestFocus();//After added the records we have to clear the text boxes 
						//The Mouse cursor should be focused on the Book Name text box
				}
				
				catch (SQLException el) {
					
				}
				
			}
		});
		btnNewButton.setBounds(25, 337, 117, 53);
		frame.getContentPane().add(btnNewButton);
		
		JButton btnClear = new JButton("Clear");
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				//Clears the form
				
				txtbname.setText("");
				txtedition.setText("");
				txtprice.setText("");
				txtbname.requestFocus();
				
			}
		});
		btnClear.setBounds(287, 337, 117, 53);
		frame.getContentPane().add(btnClear);
		
		JButton btnNewButton_1_1 = new JButton("Exit");
		btnNewButton_1_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				System.exit(0); //Closes the Form
				
			}
		});
		btnNewButton_1_1.setBounds(158, 337, 117, 53);
		frame.getContentPane().add(btnNewButton_1_1);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(478, 121, 382, 281);
		frame.getContentPane().add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(null, "Search", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.setBounds(25, 410, 412, 76);
		frame.getContentPane().add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblNewLabel_1_1_2 = new JLabel("Book ID");
		lblNewLabel_1_1_2.setBounds(26, 29, 57, 17);
		lblNewLabel_1_1_2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		panel_1.add(lblNewLabel_1_1_2);
		
		txtbid = new JTextField();
		txtbid.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				
				try {
					
					String id = txtbid.getText(); //The typing id is added to id variable
					
						pst = con.prepareStatement("select name,edition,price from book where id = ?"); //The question mark will be filled with the entered ID
						pst.setString(1, id); //Pass the id that you need to enter
						ResultSet rs = pst.executeQuery();//Executes the query, ResultSet
						
					if(rs.next()==true) //if the entered record is available its pass to the relevant text field
					{
						
						String name = rs.getString(1);
						String edition = rs.getString(2);
						String price = rs.getString(3);
						
						txtbname.setText(name); //It assigns to the relevant text fields
						txtedition.setText(edition);
						txtprice.setText(price);
						
						
					}
					else
					{
						txtbname.setText(""); //If their is no record it will be blank
						txtedition.setText("");
						txtprice.setText("");
						
					}
					
					
				}
				catch(SQLException ex){
					
				}
				
			}
		});
		txtbid.setColumns(10);
		txtbid.setBounds(85, 22, 207, 32);
		panel_1.add(txtbid);
		
		JButton btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String bname,edition,price,bid; //Every text added to the text boxes are inserted to this variables
				
				bname = txtbname.getText(); 
				edition = txtedition.getText();
				price = txtprice.getText();
				bid = txtbid.getText();
				
				try {
						pst = con.prepareStatement("update book set name= ?,edition= ?,price= ? where id =?"); //Code for Update the records in to the database
						pst.setString(1, bname);
						pst.setString(2, edition);
						pst.setString(3, price);
						pst.setString(4, bid);
						pst.executeUpdate();
						JOptionPane.showMessageDialog(null, "Record Update!!!!!");
						table_load(); //After is the successful message the tabl_load() method will be loaded, means the table should be refreshed
						
						txtbname.setText("");
						txtedition.setText("");
						txtprice.setText("");
						txtbid.setText("");
						txtbname.requestFocus();//After added the records we have to clear the text boxes 
						//The Mouse cursor should be focused on the Book Name text box
				}
				
				catch (SQLException el) {
					
					el.printStackTrace();
					
				}
				
			}
			
				
		});
		btnUpdate.setBounds(550, 410, 117, 53);
		frame.getContentPane().add(btnUpdate);
		
		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String bid; //Every text added to the text boxes are inserted to this variables
				
				
				bid = txtbid.getText();
				
				try {
						pst = con.prepareStatement("delete from book where id =?"); //Code for Update the records in to the database
						pst.setString(1, bid);
						pst.executeUpdate();
						JOptionPane.showMessageDialog(null, "Record Delete!!!!!");
						table_load(); //After is the successful message the tabl_load() method will be loaded, means the table should be refreshed
						
						txtbname.setText("");
						txtedition.setText("");
						txtprice.setText("");
						txtbid.setText("");
						txtbname.requestFocus();//After added the records we have to clear the text boxes 
						//The Mouse cursor should be focused on the Book Name text box
				}
				
				catch (SQLException el) {
					
					el.printStackTrace();
					
				}
				
			}
			
		});
		btnDelete.setBounds(679, 410, 117, 53);
		frame.getContentPane().add(btnDelete);
	}
}







