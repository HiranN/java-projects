import java.time.LocalDate;
import java.time.Period;

public class AgeCalc {

	public static void main(String[] args) {
		
		LocalDate today = LocalDate.now(); //Local Date
		LocalDate birthDate = LocalDate.of(1998, 8, 6); //Input the user Birthday
		int years = Period.between(birthDate, today).getYears(); //To find the difference between today and birthDate we put them to Period
		
		System.out.println(today);
		System.out.println(birthDate);
		System.out.println(years);

	}

}

//Age Calculator program from Date of Birth
